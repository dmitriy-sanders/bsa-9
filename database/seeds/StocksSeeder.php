<?php

use Illuminate\Database\Seeder;

class StocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Entities\User::all();

        foreach ($users as $user) {
            factory(\App\Entities\Stock::class, 3)->create([
                'user_id' => $user->id
            ]);
        }
    }
}
