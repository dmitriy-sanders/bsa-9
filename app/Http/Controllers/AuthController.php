<?php

namespace App\Http\Controllers;

use App\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['login', 'registration']]);
    }

    public function login()
    {
        $email = request('email');
        $password = request('password');

        if(!$email || !$password) {
            return response()->json([
                "error" => "Invalid data"
            ], 400);
        }
        if (! $token = auth()->attempt(['email' => $email, 'password' => $password])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }


        return $this->respondWithToken($token);
    }

    public function registration()
    {
        $name = request('name');
        $email = request('email');
        $password = request('password');

        if(!$name || !$email || !$password) {
            return response()->json([
                "error" => "Invalid data"
            ], 400);
        }

        if (User::where("email", $email)->get()->count()) {
            return response()->json([
                "error" => "User with such email already exists!"
            ], 400);
        }

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->save();

        return response()->json(['message' => 'Successfully registration!']);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
