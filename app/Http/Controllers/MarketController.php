<?php

namespace App\Http\Controllers;

use App\Entities\Stock;
use App\Services\Exceptions\InvalidFrequencyException;
use Illuminate\Http\Request;
use App\Actions\{
    CreateStockAction,
    DeleteStockAction,
    GetChartDataAction,
};
use App\Actions\Requests\{
    CreateStockRequest,
    DeleteStockRequest,
    GetChartDataRequest,
};

class MarketController extends Controller
{
    private CreateStockAction $createStockAction;
    private GetChartDataAction $getChartDataAction;
    private DeleteStockAction $deleteStockAction;

    public function __construct(
        CreateStockAction $createStockAction,
        GetChartDataAction $getChartDataAction,
        DeleteStockAction $deleteStockAction
    ) {
        $this->createStockAction = $createStockAction;
        $this->getChartDataAction = $getChartDataAction;
        $this->deleteStockAction = $deleteStockAction;

    }

    public function createStock(Request $request)
    {
        $start_date_timestamp = (new \DateTime($request->start_date))->getTimestamp();

        if ($start_date_timestamp <= time()) {
            return response()->json([
                "error" => "start_date must be greater than now_date"
            ], 400);
        }

        if ($request->price <= 0) {
            return response()->json([
                "error" => "Price must be greater than 0."
            ], 400);
        }

        $result = $this->createStockAction->execute(
            new CreateStockRequest(
                new \DateTime($request->start_date),
                floatval($request->price)
            )
        );

        return response()->json([
            "data" => $result->toArray()
        ], 201);
    }

    public function deleteStock(Request $request)
    {
        $result = $this->deleteStockAction->execute(
            new DeleteStockRequest($request->stockId)
        );

        return response()->json([
            "data" => $result->toArray()
        ], 204);
    }

    public function getChartData(Request $request)
    {
        if ((float)$request->start_date > (float)$request->end_date) {
            return response()->json([
                "error" => "start_date must be less than end_date"
            ], 400);
        }

        if ((float)$request->frequency <= 0) {
            return response()->json([
                "error" => InvalidFrequencyException::ERROR_MESSAGE
            ], 400);
        }

        $result = $this->getChartDataAction->execute(
            new GetChartDataRequest(
                new \DateTime('@' . $request->start_date),
                new \DateTime('@' . $request->end_date),
                intval($request->frequency),
            )
        );

        return response()->json([
            "data" => $result->toArray()
        ], 200);
    }
}
