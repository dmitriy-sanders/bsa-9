<?php

namespace App\Actions\Exceptions;

class InvalidPriceException extends \InvalidArgumentException
{
    const ERROR_MESSAGE = 'Price must be greater than 0';

    public function __construct()
    {
        parent::__construct(self::ERROR_MESSAGE);
    }
}
