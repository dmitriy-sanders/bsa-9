<?php

namespace Tests\Feature;

use App\Services\Exceptions\InvalidFrequencyException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MarketApiTest extends TestCase
{
    use DatabaseMigrations;

    public function test_register_user()
    {
        $userData = [
            "name"      => "TestUser",
            "email"     => "test_email@gmail.com",
            "password"  => "password"
        ];

        $response = $this->post("/api/auth/register", $userData);

        $response
            ->assertJson([
            "message" => "Successfully registration!"
        ])
            ->assertStatus(200);
    }

    public function test_register_user_invalid_data()
    {
        $userData = [
            "name"      => "TestUser",
            "email"     => "test_email@gmail.com",
            "password"  => ""
        ];

        $response = $this->post("/api/auth/register", $userData);

        $response
            ->assertJson([
                "error" => "Invalid data"
            ])
            ->assertStatus(400);
    }

    public function test_register_user_already_exists()
    {
        $this->register_user();

        $userData = [
            "name"      => "TestUser",
            "email"     => "test_email@gmail.com",
            "password"  => "password"
        ];

        $response = $this->post("/api/auth/register", $userData);

        $response
            ->assertJson([
                "error" => "User with such email already exists!"
            ])
            ->assertStatus(400);
    }

    public function test_login_user_fail()
    {
        $userData = [
            "email"     => "test_email_123@gmail.com",
            "password"  => "password"
        ];

        $response = $this->post("/api/auth/login", $userData);

        $response
            ->assertJson([
                "error" => "Unauthorized"
            ])
            ->assertStatus(401);
    }

    public function test_login_user_success()
    {
        $response = $this->authenticate();

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "access_token",
                "token_type",
                "expires_in"
            ]);

        $this->token = $response["access_token"];
    }

    public function test_creating_data_unauthorized_user()
    {
        $response = $this->post("/api/stocks", [
            "price" => 8.1,
            "start_date" => "2020-10-21 11:28:19"
        ]);

        $response->assertStatus(401);
    }

    public function test_creating_data_authorized_user()
    {
        $response = $this->authenticate();

        $response = $this
            ->withHeader("Authorization", "Bearer " . $response["access_token"])
            ->post("/api/stocks", [
                "price" => 8.1,
                "start_date" => "2020-10-21 11:28:19"
            ]);

        $response
            ->assertJson([
                "data" => [
                    "id" => 1,
                    "price" => 8.1,
                    "start_date" => "2020-10-21 11:28:19"
                ]
            ])
            ->assertStatus(201);
    }

    public function test_createStock_start_date_less_than_now_date()
    {
        $response = $this->authenticate();

        $response = $this
            ->withHeader("Authorization", "Bearer " . $response["access_token"])
            ->post("/api/stocks", [
                "price" => 88.1,
                "start_date" => "2020-01-21 11:28:19"
            ]);

        $response
            ->assertJson([
                "error" => "start_date must be greater than now_date"
            ])
            ->assertStatus(400);
    }

    public function test_deleting_data_unauthorized_user()
    {
        $response = $this->delete("/api/stocks/1");
        $response->assertStatus(401);
    }

    public function test_deleting_wrong_data_authorized_user()
    {
        $this->setData();
        $response = $this->authenticate();

        $response = $this
            ->withHeader("Authorization", "Bearer " . $response["access_token"])
            ->delete("/api/stocks/1");
        $response
            ->assertJson([
                "message" => "Stock not found"
            ])
            ->assertStatus(404);
    }

    public function test_getChartData()
    {
        $this->setData();
        $response = $this->get("/api/chart-data?start_date=4348800&end_date=1582185600&frequency=86400");

        $response
            ->assertJsonStructure([
                "data" => []
            ])
            ->assertStatus(200);
    }

    public function test_getChartData_start_date_less_than_end_date()
    {
        $this->setData();
        $response = $this->get("/api/chart-data?start_date=1582185600&end_date=4348800&frequency=86400");

        $response
            ->assertJson([
                "error" => "start_date must be less than end_date"
            ])
            ->assertStatus(400);
    }

    public function test_getChartData_frequency_less_than_zero()
    {
        $this->setData();
        $response = $this->get("/api/chart-data?start_date=4348800&end_date=1582185600&frequency=-86400");

        $response
            ->assertJson([
                "error" => InvalidFrequencyException::ERROR_MESSAGE
            ])
            ->assertStatus(400);
    }

    public function test_price_less_than_zero_create_stock()
    {
        $response = $this->authenticate();

        $response = $this
            ->withHeader("Authorization", "Bearer " . $response["access_token"])
            ->post("/api/stocks", [
                "price" => -8.1,
                "start_date" => "2020-10-21 11:28:19"
            ]);

        $response->assertJson([
            "error" => "Price must be greater than 0."
        ])->assertStatus(400);
    }

    public function setData()
    {
        factory(\App\Entities\User::class, 3)->create();


        $users = \App\Entities\User::all();

        foreach ($users as $user) {
            factory(\App\Entities\Stock::class, 3)->create([
                "user_id" => $user->id
            ]);
        }
    }

    /**
     * Authenticates user
     * Returns access_token, token_type, expires_in
     */
    public function authenticate()
    {
        $this->register_user();
        return $this->login_user();
    }

    /**
     * Method for registering user, writes to DB.
     */
    public function register_user()
    {
        $userData = [
            "name"      => "TestUser",
            "email"     => "test_email@gmail.com",
            "password"  => "password"
        ];

        $response = $this->post("/api/auth/register", $userData);
        return $response;
    }

    /**
     * Method for logging user, returns token.
     */
    public function login_user()
    {
        $userData = [
            "email"     => "test_email@gmail.com",
            "password"  => "password"
        ];

        $response = $this->post("/api/auth/login", $userData);
        return $response;
    }
}
