<?php

namespace Tests\Unit;

use App\DTO\Collections\ChartDataCollection;
use App\DTO\Values\ChartData;
use App\Entities\Stock;
use App\Repositories\StockRepository;
use App\Services\Exceptions\InvalidFrequencyException;
use Tests\TestCase;
use App\Services\MarketDataService;

class MarketServiceTest extends TestCase
{
    public MarketDataService $marketDataService;
    public StockRepository $stockRepository;

    public function get_function_params_valid()
    {
        return [
            [
                new \DateTime('2020-01-01 00:00:00'),
                new \DateTime('2020-01-04 00:00:00'),
                86400
            ]
        ];
    }

    public function get_function_params_invalid_frequency()
    {
        return [
            [
                new \DateTime('2020-01-01'),
                new \DateTime('2020-01-04'),
                -100
            ]
        ];
    }

    public function get_data_for_chart()
    {
        return collect([
            new Stock([
                'user_id' => 1,
                'price' => 1.7,
                'start_date' => new \DateTime('2020-01-01 10:11:00')
            ]),
            new Stock([
                'user_id' => 1,
                'price' => 1.5,
                'start_date' => new \DateTime('2020-01-01 10:11:00') // 1577902260
            ]),
            new Stock([
                'user_id' => 1,
                'price' => 9.7,
                'start_date' => new \DateTime('2020-01-01 18:11:00') // 1577902260
            ]),
            new Stock([
                'user_id' => 1,
                'price' => 3.5,
                'start_date' => new \DateTime('2020-01-02 10:11:00') // 1577988660
            ]),
            new Stock([
                'user_id' => 1,
                'price' => 2.7,
                'start_date' => new \DateTime('2020-01-02 10:11:00') // 1578008160
            ]),
        ]);
    }

    public function get_empty_data_for_chart()
    {
        return new ChartDataCollection([]);
    }

    /**
     * @dataProvider get_function_params_valid
     * @covers       \App\Services\MarketDataService::getChartData
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param int $frequency
     *
     */
    public function test_getChartData_returns_data_correctly_by_hour(
        \DateTime $startDate,
        \DateTime $endDate,
        int $frequency
    )
    {
        $this->stockRepository = $this->createMock(StockRepository::class);

        $this->stockRepository
            ->method('findByCriteria')
            ->willReturn(
                $this->get_data_for_chart()
            );

        $this->marketDataService = new MarketDataService($this->stockRepository);

        $data = $this->marketDataService->getChartData($startDate, $endDate, $frequency);

        $this->assertEquals(new ChartDataCollection([
            new ChartData(
                new \DateTime('2020-01-01 00:00:00'),
                4.3
            ),

            new ChartData(
                new \DateTime('2020-01-02 00:00:00'),
                3.1
            ),
        ]), $data);
    }

    /**
     * @dataProvider get_function_params_invalid_frequency
     * @covers       \App\Services\MarketDataService::getChartData
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param int $frequency
     */

    public function test_getChartData_throws_invalid_frequency_exception(
        \DateTime $startDate,
        \DateTime $endDate,
        int $frequency
    )
    {
        $this->stockRepository = $this->createMock(StockRepository::class);

        $this->stockRepository
            ->method('findByCriteria')
            ->willReturn(
                $this->get_data_for_chart()
            );

        $this->marketDataService = new MarketDataService($this->stockRepository);
        $this->expectException(InvalidFrequencyException::class);

        $this->marketDataService->getChartData($startDate, $endDate, $frequency);
    }

    /**
     * @dataProvider get_function_params_valid
     * @covers       \App\Services\MarketDataService::getChartData
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param int $frequency
     */
    public function test_getChartData_empty_data(
        \DateTime $startDate,
        \DateTime $endDate,
        int $frequency
    )
    {
        $this->stockRepository = $this->createMock(StockRepository::class);

        $this->stockRepository
            ->method('findByCriteria')
            ->willReturn(
                $this->get_empty_data_for_chart()
            );

        $this->marketDataService = new MarketDataService($this->stockRepository);

        $data = $this->marketDataService->getChartData($startDate, $endDate, $frequency);
        $this->assertEquals(
            new ChartDataCollection(),
            $data
        );
    }

    /**
     * @dataProvider get_function_params_valid
     * @covers       \App\Services\MarketDataService::getChartData
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param int $frequency
     */
    public function test_getChartData_invalid_date(
        \DateTime $startDate,
        \DateTime $endDate,
        int $frequency
    )
    {
        $this->assertGreaterThan($startDate, $endDate);
    }
}
